# coding utf-8

from unittest import TestCase

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../src'))

import main


class TestMain(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    # ピタゴラス定理のテスト
    def test_pitagoras(self):
        self.assertEqual(25, main.pitagoras(3, 4))

    # ２次方程式一般解のテスト（各項の係数が０以外の場合）
    def test_quadraticFormula_1(self):
        answer_list = main.quadraticFormula(1, -3, 2)

        self.assertEqual(2, len(answer_list))
        self.assertEqual(1, answer_list[0])
        self.assertEqual(2, answer_list[1])

    # ２次方程式一般解のテスト（一次項(x)の係数が０の場合）
    def test_quadraticFormula_2(self):
        answer_list = main.quadraticFormula(1, 0, -4)

        self.assertEqual(2, len(answer_list))
        self.assertEqual(-2, answer_list[0])
        self.assertEqual(2, answer_list[1])
