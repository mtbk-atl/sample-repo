from math import sqrt


def main():
    pass

# ピタゴラスの定理の答えを返却
def pitagoras(x, y):
    return x*x + y*y

# ２次方程式の一般解を返却
def quadraticFormula(a, b, c):    
    answer_route = sqrt(b*b - 4*a*c)

    answer_list = []

    answer_list.append((-b - answer_route) / (2*a))
    answer_list.append((-b + answer_route) / (2*a))

    return answer_list   


if __name__ == "__main__":
    main()

